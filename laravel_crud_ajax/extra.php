  <!-- 1st part <table class="table">
                    <thead class=" text-primary">
                      <th>IMAGE</th>
                      <th>FIRST NAME</th>
                      <th>LAST NAME</th>
                      <th>MOBILE</th>
                      <th>EMAIL</th>
                      <th>GENDER</th>
                      <th>ACTION</th>
                      <th>STATS</th>
                    </thead>
                    <tbody id="searchDataTable">
                      
                      @foreach ($users as $user)  
                        <div class="modal" id="myModal{{ $user->id }}">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            
                              <!- Modal Header ->
                              <div class="modal-header">
                                <h4 class="modal-title">Update User</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              
                              <!- Modal body ->
                              <div class="modal-body">
                                {{ Form::open(array('url' => url('/update_user'),'files' => true)) }}
                                @csrf  
                                {!! Form::hidden('id', $user->id  , array('class' => 'form-control')) !!}                          
                                <center>   
                                <img src="img/{{$user->profile_image}}" alt=""> <br><br></center>
                                {!! Form::label('First Name', '' , array('class'=>'form-lable')) !!}
                                {!! Form::text('firstName', $user->name  , array('class' => 'form-control')) !!}
                                <br><br>

                                {!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}
                                {!! Form::text('lastName', $user->lastName, array('class' => 'form-control')) !!}
                                <br><br>

                                {!! Form::label('Mobile Number', '' , array('class'=>'form-lable')) !!}
                                {!! Form::number('mobileNumber', $user->mobileNumber,array('class' => 'form-control')) !!}
                                <br> <br>

                                {!! Form::label('Gender', '' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
                                {{ Form::radio('gender', 'male', ($user->gender)=='male'? true : false) }}
                                {!! Form::label('Male' ,'' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
                                {{ Form::radio('gender', 'female' , ($user->gender)=='female'? true : false) }}
                                {!! Form::label('Female' ,'', array('class'=>'form-lable')) !!}
                                <br><br>
                                
                                {!! Form::label('Email', '' , array('class'=>'form-lable')) !!}
                                {!! Form::email('email', $user->email,array('class' => 'form-control')) !!}
                                <br><br>

                                {!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!} <br><br>
                                {!! Form::file('image',array('class' => 'form-control')) !!}
                              </div>
                             <center> {!! Form::submit('submit',array('class'=>'btn-submit')); !!}</center>
                              <!- Modal footer ->
                             <br><br>
                              {!! Form::close() !!}
                            </div>
                          </div>
                        </div>
                      <tr>
                        
                        <td><img src="img/{{$user->profile_image}}"></td>
                      	<td>{{ $user->name }}</td>
                        <td>{{ $user->lastName }}</td>
                        <td>{{ $user->mobileNumber }}</td>
                        <td>{{ $user->gender }}</td>
                        <td>{{ $user->email }}</td>
                        <td><a href="" data-toggle="modal" data-target="#myModal{{ $user->id }}" ><i class="fas fa-user-edit"></i></a> &nbsp;
                         <a href="/delete_user/{{$user->id}}"><i class="far fa-trash-alt"></i></a></td>
                         @if($user->status == 0)
                            <td><a href='/update_user/{{$user->id}}/{{$user->status}}' class='btn btn-danger'>Deactive</a></td>
                        @else
                             <td><a href='/update_user/{{$user->id}}/{{$user->status}}' class='btn btn-info'>Active</a></td>
                        @endif
                      </tr>
                   @endforeach
                      
                    </tbody>
                  </table> -->