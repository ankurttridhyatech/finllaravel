<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\admin;
use Session;
class ProfileViewController extends Controller
{
    public function profileView()
    {
    	//$id = session()->get('LoginId');
    	$user = new admin();
        $users = $user::find(1);
        
		return view('admin.profileView',['user'=>$users]);
    }

    public function userProfileView()
    {
    	
    	$users = Auth::user();
        return view('user.profileView',['user'=>$users]);
    }
}
