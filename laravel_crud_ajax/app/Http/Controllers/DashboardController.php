<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use DB;

class DashboardController extends Controller
{
    public function dashboard(Request $request)
    {
       //$record = User::all();
        //echo json_encode($record);
        $getAllUser = User::select('name','lastname','id')->get() ;

        $record = User::sortable()->paginate(2);
         //$record = User::where('status',0);
         if($request->dropdown && $request->dropdown!= '' ){
            $record->where('id',$request->dropdown);
           
         }
          if($request->email && $request->email!= '' ){
                $record->where('email','LIKE',$request->email);
          }
          if($request->number && $request->number!= '' ){
                $record->where('mobileNumber',$request->number);
          }
          if($request->gender && $request->gender!= '' ){
                $record->where('gender',$request->gender);
          }
        
        // $data = $record->sortable()->paginate(2);
         return view("admin.index",['record'=>$record,'getAllUser'=>$getAllUser,'request'=>$request])->render();

        // echo "<pre>";print_R($record->toSql() );exit;
    }

    public function userDashboard()
    {
		return view('user.index');

    }
}
