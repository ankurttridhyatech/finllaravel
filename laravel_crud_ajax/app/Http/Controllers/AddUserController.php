<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Illuminate\Support\Facades\File;
class AddUserController extends Controller
{
    public function addUserForm()
    {
    	return view('admin.addUserForm');
    }

    public function addUserData(Request $data)
    {

        $validator=Validator::make($data->all(),[
            'name' => 'required|max:30|min:3',
            'lastName' => 'required|max:30|min:3',
            'gender' => 'required',
            'mobileNumber' => 'required|digits:10',
            'address'=> 'required|max:100|min:15',
            'zipcode'=> 'required|digits:06',
            'email' => 'required|email|unique:users,email',
            'Password' => 'required|confirmed|min:6',
            'image' => 'required',
            'status' => 'required',
        ]);

         if($validator->fails())
         {
          //return redirect('/add_user')->withErrors($validator)->withInput();
            echo json_encode(["ans"=>"0","error"=>$validator->errors()]);

                        
         }

         else{
        
        $users = new User();
        $t=time();
        $img = $t.'_'.$data->file('image')->getClientOriginalName();

    	$users->name = $data->name;
    	$users->lastName = $data->lastName;
    	$users->gender = $data->gender;
    	$users->mobileNumber = $data->mobileNumber;
        $users->address = $data->address;
        $users->zipcode = $data->zipcode;
    	$users->email = $data->email;
    	$users->profile_image = $img;
        $users->password = Hash::make($data->Password);
    	$users->status = $data->status;

        $users->save();
        $data->file('image')->move('img/',$img);
        
        echo json_encode(["ans"=>"1"]);

    }
    	
    }

    public function updateUserData(Request $data)
    {   
        $validator=Validator::make($data->all(),[
            'name' => 'required|max:30|min:3',
            'lastName' => 'required|max:30|min:3',
            'gender' => 'required',
            'mobileNumber' => 'required|digits:10',
            'address'=> 'required|max:100|min:15',
            'zipcode'=> 'required|digits:06',
            'email' => 'required|email|unique:users,email,'.$data->id.',id',
           
        
        ]);

         if($validator->fails())
         {
          //return redirect('/add_user')->withErrors($validator)->withInput();
            echo json_encode(["ans"=>"0","error"=>$validator->errors()]);
            
         }

         else{
        
        $user = new User();
        $id = $data->id;
    
        $users = $user::find($id);

        $users->name = $data->name;
        $users->lastName = $data->lastName;
        $users->address = $data->address;
        $users->zipcode = $data->zipcode;
        $users->mobileNumber = $data->mobileNumber;
        $users->email = $data->email;
       
        if($data->has('image'))
        {
            $t=time();
            $img = $t.'_'.$data->file('image')->getClientOriginalName();
            $users->profile_image = $img;
            $data->file('image')->move('img/',$img);
        }

        $users->save();
        echo json_encode(["ans"=>1]);

        //echo json_encode(["ans"=>1]);
        //return redirect('/dashboard');
    }
}
    public function updateUserProfile(Request $data)
    {
        $user = new User();
        $id = $data->id;

        

        $users = $user::find($id);

        $users->name = $data->name;
        $users->lastName = $data->lastName;
         $users->address = $data->address;
        $users->zipcode = $data->zipcode;
        $users->mobileNumber = $data->mobileNumber;
        $users->email = $data->email;
        $users->gender = $data->gender;
        if($data->has('image'))
        {
            $t=time();
            $img = $t.'_'.$data->file('image')->getClientOriginalName();
            $users->profile_image = $img;
            $data->file('image')->move('img/',$img);
        }

        $users->save();
        return redirect('/user_profile_view');
    }


    public function updateUserStatus($id,$status)
    {
       

        $users = User::find($id);        

        if($status == 0)
        {
            $users->status = 1;
        }
        else
        {
            $users->status = 0;
        }

        $users->save();
        return redirect("/dashboard");
    }

    public function DeleteUserData($id)
    {

        //$id=$request->id;
        // find user for deletes
        User::destroy($id);
       
       // if(isset($response))
       // {
       //      echo  json_encode(["ans"=>1]);
       // }
       // else
       // {
       //      echo  json_encode(["ans"=>0]);

       // }
       

        // $id=$request->id;
        // $user = new User();
        // $users = $user::find($id);
        // echo $id;
        // exit;
        // $image_path =  "img/".$users->profile_image;

        //  if(File::exists($image_path)) {
        //          File::delete($image_path);
        //     }
        // $users->delete();
        //     echo "ok";
        return redirect('/dashboard');
    }
}


