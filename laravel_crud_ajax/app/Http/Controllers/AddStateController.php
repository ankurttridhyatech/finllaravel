<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection ;
use Illuminate\Http\Request;
use App\State;
use Validator;

class AddStateController extends Controller
{
    public function addState()
    {   
   
        $users = State::Paginate(3);
    	return view('admin.addStateForm',['users' => $users]);
    }
    public function addStateData(Request $stateForm)
    {
    	   
     $validator=Validator::make($stateForm->all(),[
            'stateName' => 'required|unique:state,name',
        ]);

         if($validator->fails())
         {
          
           return redirect("/add_state")->withErrors($validator)->withInput();
         }
         else
          {
           $stateTable = new State();
           $stateTable->id = $stateForm->id;
    	   $stateTable->name = $stateForm->stateName;
    	   $stateTable->save();
    	   return redirect('add_state');
          }
    }
public function ajax_editeUserDataa(Request $request)
    {
            $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:state,name',
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else
        {

            $user = state::find($request->id);
            $user->name     = $request->name;
          
            $response       = $user->save();

             
            echo json_encode(["ans"=>1]);
        }

    }

   
    public function deleteStateData($id)
    {
             State::destroy($id);
             return redirect('add_state');
    }

}
