<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\State;
use Validator;


class AddCityController extends Controller
{
    public function addCity()
    {	

    	$data = State::all();
    	$req1 = City::all();

    	
    	$req1 = City::Paginate(3);
    	return view('admin.addCityForm',['req1' => $req1,'states' => $data]);
    	
    }

    public function addCityData(Request $data)
    {   

    	    $validator=Validator::make($data->all(),[
            'cityname' => 'required|unique:city,city_name',
            'stateName' => 'required',
        ]);

         if($validator->fails())
         {
          
           return redirect("/add_city")->withErrors($validator)->withInput();
         }
         else
        {
           $city = new City();
    	   $city->state_name = $data->stateName;
    	   $city->city_name = $data->cityname;
    	   $city->save();
    	   return redirect('/add_city');
    	}
    }

   public function ajax_editeUserDataa(Request $request)
    {
    
            $user = state::find($request->id);
            $user->name     = $request->name;
          
            $response       = $user->save();

            echo json_encode(["ans"=>1]);
    }


    public function deleteCityData($id)
    {
             
             City::destroy($id);
             return redirect('add_city');
    }

}
