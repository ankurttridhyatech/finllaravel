<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\admin;
use DB;
use Session;

class LoginController extends Controller
{

     public function admin()
    {   
        return view('admin.login');
    }
    public function loginCheck(Request $adminlogin)
    {

      $userName = $adminlogin->userName;
      $password = $adminlogin->password;
      
      $user =  admin::where('email',$userName)->where('password',$password)->first();
            $adminlogin->Session()->put('LoginId',$user['id']);

      if(! empty ($user))
          {
            return redirect("/dashboard");
          }
          else {
              return redirect("/admin");
          }
    }

    public function logout(Request $adminlogin)
    {

      $adminlogin->Session()->forget('LoginId');
      return redirect("/admin");
    }
   
}
