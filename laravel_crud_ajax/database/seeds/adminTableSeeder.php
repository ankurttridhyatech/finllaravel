<?php

use Illuminate\Database\Seeder;

class adminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert(
        	[
        		'name' => 'admin',
        		'email' => 'admin@gmail.com',
        		'password' => 'admin'
        	]);
    }
}