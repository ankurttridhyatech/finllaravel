@extends('admin.layout.master')
@section('header','Add city')
@section('title','Add city')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
 
$(document).ready(function(){
  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(".edtt").click(function(){
 
  //   var id=$(this).attr('id');  
  
      var currentRow=$(this).closest("tr"); 

       //  var img_src=currentRow.find("td:eq(0) > img").attr('src');
         var name=currentRow.find("td:eq(1)").text();
         var id=currentRow.find("td:eq(0)").text();

          $("#t1").val(name);
          $("#idt").val(id);

  });
$('#upload_userdataa').submit(function(e) {
            e.preventDefault();
          debugger;
          
           //var formData=$("#upload_userdata").serialize();
         var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "ajax_editeUserDataa",
                data: formData,
                dataType : 'json',
                cache:false,
                contentType: false,
                processData: false,
                success : function(data)
                    {
                        if(data['ans'])
                        {
                         

                         alert("update succesfully")
                               

                        }
                        
                       
                    }
            }); 
        });
});
</script> 

 <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
         <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <form action="{{route('ajax_editeUserDataa')}}" method="post"  id="upload_userdataa" enctype="multipart/form-data" class="form-group">  
            @csrf
          <div class="form-group">
                <label for="state">States:</label>
                <input type="text" id="t1" name="name">
               <input type="text" name="id"  id="idt" class="form-control">
                <p class="text-danger clean name"></p> 
                 <span class="text-danger">{{ $errors->first('stateName1') }}</span>
               </div>
            <br>

            <center><input type="submit" id="submit" value="update" class="btn btn-danger"></center>
        
        <!-- Modal footer -->
        <div class="modal-footer">
         
        </div>
        </form>
      </div>
    </div>
  </div>  

     
  
</div>

		

	{{ Form::open(array('url' => url('/add_city_data'),'files' => true, 'class'=>'card mt-5 w-100','id'=>'city_form')) }}
	<h1 class="font-color" style="text-align: center">ADD CITY</h1>
			@csrf
	  <form>
				<div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
				<div class="row justify-content-around">
			       <div class="col-4">
					    <select class="form-control" name="stateName">
    							<option value="" disabled="" selected="">Select State</option>
   							
    									@foreach ($states as $state)
           						 			<option value="{{$state->name}}">{{$state->name}}</option>
      			  						@endforeach
						</select>
								<span class="text-danger">{{ $errors->first('stateName') }}</span>
					</div>
		
					<div class="col-4">
            					<input type="name" id="name" name="cityname" placeholder="Enter city"><br><br>
               		    		<span class="text-danger">{{ $errors->first('cityname') }}</span>
              		</div>
				</div>
        </form>	     
        		 <input type="submit" class="btn btn-primary ml-5" value="ADD" >
				 <table class="table table-striped">
  						<thead>
						    <tr>
						       <th scope="col">ID</th>
						      <th scope="col">State</th>
						      <th scope="col">City</th>
						      <th scope="col">Action</th>
						    </tr>
  						</thead>
					     

					     @foreach($req1 as $data)
					        <tr>
					          <th>{{$data->id }}</th>
					          <th>{{$data->state_name }}</th>
					           <th>{{$data->city_name }}</th>
					           <th><a href="/update_city/{{$data->id}}" data-toggle="modal" data-target="#myModal" ><i class="fa fa-edit"></i></a><a href="/delete_city/{{$data->id}}"><i class=" fa fa-trash"></i></a></th>
					        </tr>
					     @endforeach


				 </table>
				 {{ $req1->links()  }}
@endsection
                         
		
                    

                          

                      
                          
                     	
                 

		

       

