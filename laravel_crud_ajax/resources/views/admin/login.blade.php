<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	@include('admin.layout.head')
</head>
<body>
<center>
	{{ Form::open(array('url' => url('/admin_login'), 'class'=>'mt-5 pt-5')) }}
	<h1 class="font-color mt-5 pt-5">Admin Login</h1>
			@csrf
	<table>
		<tbody>
			<tr>
				<td>{!! Form::label('User Name', '' , array('class'=>'form-lable')) !!}</td>
				<td>{!! Form::text('userName', '' , array('class' => 'form-control')) !!}</td>
			</tr>
	       <tr>
	       		<td>{!! Form::label('Password', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{!! Form::password('password',['class' => 'form-control']) !!}</td>
	       </tr>
		</tbody>
	</table>
       
       {!! Form::submit('submit',array('class'=>'btn-submit')); !!}
  {!! Form::close() !!}
</center>

</body>
</html>



