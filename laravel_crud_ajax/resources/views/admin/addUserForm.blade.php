@extends('admin.layout.master')
@section('header','Add user')
@section('title','Add user')
@section('content')
<style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance:textfield;
}
</style>

<center>
	{{ Form::open(array('url' => url('/add_user_data'),'files' => true, 'class'=>'card mt-5 pt-5 pb-5 w-50' ,'id'=>'add_user_form')) }}
	<h1 class="font-color">ADD USER</h1>
			@csrf
	<table>
		<tbody>
			<tr >
				<td>{!! Form::label('name', '' , array('class'=>'form-lable')) !!}</td>
				<td>{!! Form::text('name', '' , array('class' => 'form-control' , 'id' => 'name')) !!}	
            	<p class="text-danger clean name"></p>
            	</td>

			</tr>
	       <tr>	
		       <td>{!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}</td>
		       <td>{!! Form::text('lastName', '', array('class' => 'form-control' , 'id' => 'lastName')) !!}
		       <p class="text-danger clean lastName"></p>			   
	            </td>
	       </tr>
	       <tr>	
		       <td>{!! Form::label('Address', '' , array('class'=>'form-lable')) !!}</td>
		       <td>{!! Form::text('address', '', array('class' => 'form-control' , 'id' => 'address')) !!}
		       <p class="text-danger clean address"></p>			   
	            </td>
	       </tr>
	       <tr>	
		       <td>{!! Form::label('Zipcode', '' , array('class'=>'form-lable')) !!}</td>
		       <td>{!! Form::number('zipcode', '', array('class' => 'form-control' , 'id' => 'zipcode')) !!}
		       <p class="text-danger clean zipcode"></p>			   
	            </td>
	       </tr>
	       <tr>
	       		<td>{!! Form::label('Mobile Number', '' , array('class'=>'form-lable')) !!}
   				</td>
	       		<td>{!! Form::number('mobileNumber', '',array('class' => 'form-control equipCatValidation' , 'id' => 'mobileNumber')) !!}
	       		<p class="text-danger clean mobileNumber"></p>		     
            	</td>


	       </tr>
	       <tr>
	       		<td>{!! Form::label('Email', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{!! Form::email('email', '',array('class' => 'form-control' , 'id' => 'email')) !!}
	       		<p class="text-danger clean email"></p>		     
            	</td>
	       </tr>
	       <tr>
	       		<td> {!! Form::label('Gender', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{{ Form::radio('gender', 'male' , false , ['id'=>'male']) }}
       				{!! Form::label('Male' ,'' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
       				{{ Form::radio('gender', 'female' , false , ['id'=>'female']) }}
      				{!! Form::label('Female' ,'', array('class'=>'form-lable')) !!}
      				<p class="text-danger clean gender "></p>      
      			</td>
	       </tr>
	       <tr>
	       		<td>{!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{!! Form::file('image',array('class' => 'form-control' , 'id' => 'image')) !!}
	       		<p class="text-danger clean profile_image"></p>	      
	            </td>
	       </tr>
	       <tr>
	       		<td>{!! Form::label('Password', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{!! Form::password('Password',['class' => 'form-control' , 'id' => 'password']) !!}	
	       		<p class="text-danger clean Password"></p>      
	            </td>
	       </tr>
	       <tr>
	       		<td>{!! Form::label('Confirm-Password', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{!! Form::password('Password_confirmation',['class' => 'form-control' , 'id' => 'rePassword']) !!}</td>
	       		
	       </tr>
	       <tr>
	       		<td>{!! Form::label('Status', '' , array('class'=>'form-lable')) !!}</td>
	       		<td>{!! Form::select('status', array('1' => 'Active', '0' => 'Deactive'),null,array('class' => 'form-control' , 'id' => 'status')) !!}</td>
	       </tr>
		</tbody>
	</table>
       
       {!! Form::submit('submit',array('class'=>'btn-submit' , 'id' => 'submit')); !!}
  {!! Form::close() !!}
</center>



@endsection